import time
import re
import os
import json
import requests
from flask import Flask, render_template, request, jsonify

this_dir = os.path.dirname(__file__)
alnum_pattern = re.compile('[\W_]+')
STEP = 4

app = Flask(__name__)

with open(os.path.join(this_dir, 'data.json')) as infile:
    WORD_CACHE = json.load(infile)

WORD_APIS = [
    {
        "rel": "rhymes",
        "api": "rel_rhy",
        "step_score": 10,
        "nonstep_score": 4,
    },
    {
        "rel": "approx_rhymes",
        "api": "rel_nry",
        "step_score": 5,
        "nonstep_score": 2,
    },
    {
        "rel": "homophones",
        "api": "rel_hom",
        "step_score": 8,
        "nonstep_score": 3,
    },
    {
        "rel": "triggers",
        "api": "rel_trg",
        "step_score": 2,
        "nonstep_score": 5,
    },
    {
        "rel": "sounds_likes",
        "api": "sl",
        "step_score": 7,
        "nonstep_score": 3,
    },
    {
        "rel": "followers",
        "api": "rel_bga",
        "step_score": 0,
        "nonstep_score": 2,
    },
    {
        "rel": "synonyms",
        "api": "rel_syn",
        "step_score": 1,
        "nonstep_score": 3,
    },
]


@app.route("/")
def home():
    return render_template("index.html")


def external_r(word):
    global WORD_CACHE
    rv = {"enriched_word": {}, "from_cache": [], "from_external": []}
    for word_api in WORD_APIS:
        rel = word_api["rel"]
        api = word_api["api"]
        json_response = WORD_CACHE.get(word, {}).get(rel, None)
        if json_response is not None:
            rv["enriched_word"][rel] = json_response
            rv["from_cache"].append("{rel}.{word}".format(rel=rel, word=word))
            continue
        try:
            print("> start ext req: {}.{}...".format(api, word))
            api_response = requests.get("https://api.datamuse.com/words?{api}={word}".format(api=api, word=word))
            print("> end   ext req: {}.{}...".format(api, word))
            json_response = api_response.json()
        except Exception as e:
            print(">>> problem getting response from external api: {}".format(e))
        this_word_cache = WORD_CACHE.get(word, {})
        if not this_word_cache:
            WORD_CACHE[word] = {}
        WORD_CACHE[word][rel] = json_response
        with open(os.path.join(this_dir, 'data.json'), 'w') as outfile:
            try:
                json.dump(WORD_CACHE, outfile)
            except Exception as e:
                print(">>> problem updating cache: {}".format(e))
        rv["enriched_word"][rel] = json_response
        rv["from_external"].append("{rel}.{word}".format(rel=rel, word=word))
    return rv


USED_WORDS = []


def process_word(word):
    global USED_WORDS
    word = word.lower()
    word = alnum_pattern.sub('', word)
    USED_WORDS.append(word)
    return external_r(word)


def create_groups():
    prv = {}
    for used_word in USED_WORDS:
        if used_word in prv:
            continue
        word_meta = WORD_CACHE[used_word]
        group_pool = []
        for rel_word in word_meta['rhymes']:
            group_pool.append(rel_word['word'])
        prv[used_word] = group_pool
    groups = []
    for prv_word, prv_group in prv.items():
        for group in groups:
            if prv_word in group["group_pool"]:
                group["group_leaders"].add(prv_word)
                for prv_group_word in prv_group:
                    group["group_pool"].add(prv_group_word)
                break
        else:
            groups.append({
                "group_leaders": {prv_word},
                "group_pool": set(prv_group),
            })
    rv = []
    for group in groups:
        rv.append({
            "group_leaders": list(group["group_leaders"]),
            "group_pool": list(group["group_pool"]),
        })
    return rv


@app.route("/api")
def api():
    global USED_WORDS
    from_cache_cnt, from_external_cnt = 0, 0
    USED_WORDS = []
    body = request.args.get("body")
    words = body.split()
    current_word = len(words)
    enriched_words = []
    start_enriching = time.time()
    for word in words:
        processed_word = process_word(word)
        enriched_word = {"word": word, "word_meta": processed_word['enriched_word'],
                         "from_cache": processed_word["from_cache"],
                         "from_external": processed_word["from_external"],
                         }
        from_external_cnt += len(processed_word["from_external"])
        from_cache_cnt += len(processed_word["from_cache"])
        enriched_words.append(enriched_word)
    end_enriching = time.time()
    time_enriching = time.time() - start_enriching
    time_scoring = time.time() - end_enriching
    total_time = time.time() - start_enriching
    groups = create_groups()
    return jsonify({
        "from_cache_cnt": from_cache_cnt,
        "from_external_cnt": from_external_cnt,
        "time_enriching": time_enriching,
        "time_scoring": time_scoring,
        "total_time": total_time,
        "current_words": words,
        "current_words_cnt": current_word,
        "enriched_words": enriched_words,
        "groups": groups,
    })


if __name__ == "__main__":
    app.run(debug=True)
